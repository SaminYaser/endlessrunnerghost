﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Text healthText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player != null)
        {
            healthText.text = "x" + GameObject.FindGameObjectWithTag("Player")
            .GetComponent<Player>().health
            .ToString();
        }
        else
        {
            healthText.text = "x0";
        }
    }
}
