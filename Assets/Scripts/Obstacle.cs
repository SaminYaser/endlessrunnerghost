﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    public int damage = 1;
    public float speed;
    public float lifeTime = 10f;

    public GameObject effect;
    public GameObject collisionSound;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        lifeTime -= Time.deltaTime;
        if(lifeTime < 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Collision sound
            Instantiate(collisionSound, transform.position, Quaternion.identity);

            // player takes damage
            other.GetComponent<Player>().health -= damage;
            Instantiate(effect, transform.position, Quaternion.identity);
            Debug.Log(other.GetComponent<Player>().health);
            Destroy(gameObject);
        }
    }
}
