﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    private Vector2 targetPos;
    public int health = 3;
    public float Yshift;
    public float speed;
    public float maxHeight;
    public float minHeight;

    public GameObject effect;
    public GameObject destroyEffect;
    public GameObject gameOver;
    private Animator camAnim;
    private bool isDestroyed = false;
    public GameObject moveSound;

    // Start is called before the first frame update
    void Start()
    {
        camAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>();
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0 && !isDestroyed)
        {
            playerDestroyEffect();
            isDestroyed = true;

            Destroy(gameObject);
            gameOver.SetActive(true);
            
            // Disables the rendering of player
            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
            // StartCoroutine("restartGame");
        }

        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.UpArrow) && transform.position.y != maxHeight) 
        {
            Instantiate(moveSound, transform.position, Quaternion.identity);
            ShowEffect();
            CamShake();
            targetPos = new Vector2(transform.position.x, transform.position.y + Yshift);
        } else if(Input.GetKeyDown(KeyCode.DownArrow) && transform.position.y != minHeight)
        {
            Instantiate(moveSound, transform.position, Quaternion.identity);
            ShowEffect();
            CamShake();
            targetPos = new Vector2(transform.position.x, transform.position.y - Yshift);
        }
    }

    private IEnumerator restartGame()
    {
        Debug.Log("RestartGame entered.");

        Debug.Log("RestartGame wait exhausted.");
        Destroy(gameObject);
        // Code to execute after the delay
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        yield return null;
    }

    void ShowEffect()
    {
        Instantiate(effect, transform.position, Quaternion.identity);
    }

    void CamShake()
    {
        camAnim.SetTrigger("Shake");
    }

    void playerDestroyEffect()
    {
        // Add focus one point effect
        Instantiate(destroyEffect, transform.position, Quaternion.identity);
    }
}
