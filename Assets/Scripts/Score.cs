﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private float timer = 0f;
    public int score;
    public Text scoreText;
    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer * 10 >= 1.1f && player.health != 0)
        {
            score += 1;
            timer = 0f;
            scoreText.text = score.ToString();
        }
    }
}
