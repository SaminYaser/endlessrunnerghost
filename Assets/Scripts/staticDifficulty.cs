﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class staticDifficulty
{
    public static float startTimeBtwSpawn = 1.95f;
    public static float minTime = 0.65f;

    public static float StartTimeBtwSpawn
    {
        get
        {
            return startTimeBtwSpawn;
        }
        set
        {
            startTimeBtwSpawn = value;
        }
    }

    public static float MinTime
    {
        get
        {
            return minTime;
        }
        set
        {
            minTime = value;
        }
    }
}
