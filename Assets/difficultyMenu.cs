﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class difficultyMenu : MonoBehaviour
{
    public Obstacle obstacle;

    private void Start()
    {
    }

    public void easyMode()
    {
        setDifficulty(5.85f, 1.95f, 3);
    }

    public void normalMode()
    {
        setDifficulty(1.95f, 0.65f, 10);
    }

    public void hardMode()
    {
        setDifficulty(1.2f, 0.45f, 13);
    }

    public void setDifficulty (float spawnBtwTime, float spawnMinTime, int obsSpeed)
    {
        staticDifficulty.startTimeBtwSpawn = spawnBtwTime;
        staticDifficulty.minTime = spawnMinTime;
        obstacle.speed = obsSpeed;
    }
}
